Bonjour,  

Nous avons bien reçu **votre message** et nous vous apporterons *rapidement* une réponse.  
Votre demande porte le numéro %{ISSUE_ID} .

Vous pouvez répondre à ce mail pour apporter des compléments.

Merci! 

L'équipe du Garage Numérique

www.legaragenumerique.fr  
4 place Henri Matisse 75020 Paris  
06 77 77 77 77  

---

![La Danse, par H. Matisse](https://upload.wikimedia.org/wikipedia/en/2/2e/La_danse_%28I%29_by_Matisse.jpg)  

